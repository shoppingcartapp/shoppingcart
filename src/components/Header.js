import React from 'react';
import SearchIcon from '../assets/icons/SearchIcon';

function Header() {
    return (
        <nav className="navbar-nav shadow-sm bg-white navbar-light sticky-top navbar-expand-lg px-5 py-2">
            <div className="navbar-brand">Shopping Cart</div>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <a href="#" className="nav-link">Home</a>
                    </li>
                    <li className="nav-item">
                        <a href="#" className="nav-link active">Products</a>
                    </li>
                </ul>
                <form className="form-inline my-2 my-lg-0">
                    <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                    <button className="btn btn-outline-dark my-2 my-sm-0" type="submit">
                        <SearchIcon fill={'#000'} height={20} width={20} />
                    </button>
                </form>
            </div>
        </nav>
    )
}
export default Header;