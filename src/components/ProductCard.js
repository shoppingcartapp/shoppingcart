import React from 'react';

export const ProductCard = () => {
    return (
        <div className="col my-2">
            <div className="w-100 card h-100 shadow-sm p-2" >
                <img
                    className="card-img-top rounded mx-auto"
                    style={{ objectFit: 'cover' }}
                    // src={'https://images.unsplash.com/photo-1588359348347-9bc6cbbb689e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80'}
                    src={'https://images.unsplash.com/photo-1602810319428-019690571b5b?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80'}
                />
                <div className="card-body w-100 ">
                    <h5 className="card-title">Light Purple Casual Shirt</h5>
                    <p className="card-text">$20</p>

                </div>
            </div>
        </div>
    )
}