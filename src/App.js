import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Header from "./components/Header";
import ProductsListing from "./screens/ProductsListing";
import { StoreContextProvider } from "./store/StoreContext";

function App() {
  return (
    <Router>
      <StoreContextProvider>
        <Header />
        <Switch>
          <Route path={"/"} component={ProductsListing} />
        </Switch>
      </StoreContextProvider>
    </Router>
  );
}

export default App;
