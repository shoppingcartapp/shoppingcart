import React from "react";

export const StoreContext = React.createContext(null);

export const StoreContextProvider = (props) => {
  const [cart, setCart] = React.useState([
    { id: 1, name: "Test Product", description: "dummydescription" },
  ]);
  return (
    <StoreContext.Provider
      value={{
        cart,
        setCart: (val) => setCart([...cart, val]),
      }}
    >
      {props.children}
    </StoreContext.Provider>
  );
};
