import React from "react";
import { ProductCard } from "../components/ProductCard";

function ProductsListing() {

  const Products = [1, 2, 3, 4, 5, 6,7,7,7,7,7,7,7];

  return (
    <div className="container-fluid m-auto bg-light">
      <div className="row row-cols-sm-3 row-cols-lg-4 m-auto">
        {
          Products.map((item, index) => <ProductCard key={index} />)
        }
      </div>
    </div>
  );
}

export default ProductsListing;
